FROM node:slim

WORKDIR /home

ADD . ./

RUN npm install -g http-server

CMD http-server -p 80 ./
